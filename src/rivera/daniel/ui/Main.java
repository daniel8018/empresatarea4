package rivera.daniel.ui;

import rivera.daniel.bl.Camisa;
import rivera.daniel.bl.Catalogo;
import rivera.daniel.bl.Cliente;
import rivera.daniel.tl.CamisaController;
import rivera.daniel.tl.CatalogoController;
import rivera.daniel.tl.ClienteController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;

public class Main {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static ClienteController gestorCliente = new ClienteController();
    static CamisaController gestorCamisa = new CamisaController();
    static CatalogoController gestorCatalogo = new CatalogoController();

    public static void main(String[] args) throws IOException {
        int opcion = -1; //variable que almacena la opcion que selecciona el usuario del menu.
        do {
            System.out.println("----------------------------------");
            out.println("1.Crear archivo cliente");
            out.println("2.guardar un Cliente");
            out.println("3. imprimir Clientes");
//---------------------------------------------------------------------
            System.out.println("----------------------------------");
            out.println("4.Crear archivo camisa");
            out.println("5.guardar una Camisa");
            out.println("6. imprimir camisas");
//---------------------------------------------------------------------
            System.out.println("----------------------------------");
            out.println("7.Crear archivo catalogo");
            out.println("8.guardar un Catalogo");
            out.println("9. imprimir Catalogos");
//---------------------------------------------------------------------
            out.println("10. Salir");

            out.println("Digite la opción: ");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);

        } while (opcion != 10);
    }

    public static void procesarOpcion(int pOpcion) throws IOException {
        switch (pOpcion) {
            case 1:
                gestorCliente.crearArchivoClientes();
                break;
            case 2:
                guardarCliente();
                break;

            case 3:
                imprimirListaClientes();
                break;
            case 4:
                gestorCamisa.crearArchivoCamisas();
                break;
            case 5:
                guardarCamisa();
                break;

            case 6:
                imprimirListaCamisas();
                break;

            case 7:
                gestorCatalogo.crearArchivoCatalogos();
                break;
            case 8:
                guardarCatalogo();
                break;

            case 9:
                imprimirListaCatalogos();
                break;

            case 10:
                break;
            default:
                out.println("Opcion inválida");
                break;

        }
    }

    public static void guardarCliente() throws IOException {
        out.println("Ingrese la cedula del cliente");
        String cedula = in.readLine();
        out.println("Ingrese el nombre del cliente");
        String nombre = in.readLine();
        out.println("Ingrese el primer apellido  del cliente");
        String apellido1 = in.readLine();
        out.println("Ingrese el segundo apellido  del cliente");
        String apellido2 = in.readLine();
        out.println("Ingrese el correo del cliente");
        String correo = in.readLine();
        out.println("Ingrese la direccion exacta de donde quiere recibir su pedido");
        String dirreccionExacta = in.readLine();

        if (gestorCliente.averiguarCliente(cedula) == null) {
            String resultado = gestorCliente.registrarCliente(nombre, apellido1, apellido2, dirreccionExacta, correo, cedula);
            System.out.println(resultado);
        }else{
            System.out.println("Cliente ya registrado anteriormente");
        }

    }


    public static void imprimirListaClientes(){
        ArrayList<Cliente> lista = gestorCliente.listarClientes();
        for (Cliente cliente : lista){
            System.out.println(cliente);
        }
    }

//---------------------------------------------------------------------
    public static void guardarCamisa() throws IOException {
        out.println("Ingrese el tamaño de la camisa");
        String tam = in.readLine();
        out.println("Ingrese la descripcion del producto");
        String descripcion = in.readLine();
        out.println("Ingrese el color de la camisa");
        String color = in.readLine();
        out.println("Ingrese el id de la camisa");
        int id = Integer.parseInt(in.readLine());

        out.println("Ingrese el precio de la camisa ");
        double precio = Double.parseDouble(in.readLine());

        if (gestorCamisa.averiguarCamisa(id) == null) {
            String resultado = gestorCamisa.registrarCamisa(tam,descripcion, color,id,precio);
            System.out.println(resultado);
        }else{
            System.out.println("Camisa ya registrada anteriormente");
        }

    }

    public static void imprimirListaCamisas(){
        ArrayList<Camisa> lista = gestorCamisa.listarCamisas();
        for (Camisa camisa : lista){
            System.out.println(camisa);
        }
    }
//---------------------------------------------------------------------

    public static void guardarCatalogo() throws IOException {
        out.println("Ingrese el id del catalogo");
        String id = in.readLine();
        out.println("Ingrese nombre del mes de creacion del catalogo");
        String nombreDelMeS = in.readLine();
        out.println("Ingrese la fecha de creación del catalogo");
        String fecha = in.readLine();
        if (gestorCatalogo.averiguarCatalogo(id) == null) {
            String resultado = gestorCatalogo.registrarCatalogo(id,nombreDelMeS,fecha);
            System.out.println(resultado);
        }else{
            System.out.println("Catalogo ya registrado anteriormente");
        }

    }

    public static void imprimirListaCatalogos(){
        ArrayList<Catalogo> lista = gestorCatalogo.listarCatalogos();
        for (Catalogo catalogo : lista){
            System.out.println(catalogo);
        }
    }
    //---------------------------------------------------------------------
    public static void agregarClienteCatalogo () throws IOException {
        System.out.print("Ingrese el id del catalogo: ");
        String id = in.readLine();
        out.println("Ingrese la cedula del cliente ");
        String cedula = in.readLine();

        if (gestorCatalogo.averiguarCatalogo(id) != null) {
            if (gestorCliente.averiguarCliente(cedula) != null) {
                String respuesta = gestorCatalogo.agregarClienteACatalogo(cedula,id);
                out.println(respuesta);
            } else {
                out.println("El cliente con id  " + id + " no se encuentra");
            }
        } else {
            out.println("El catalogo con id  " + id + " no se encuentra");
        }
    }
}
