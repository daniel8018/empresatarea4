package rivera.daniel.dl;

import rivera.daniel.bl.Camisa;
import rivera.daniel.bl.Catalogo;
import rivera.daniel.bl.Cliente;

import java.io.*;
import java.util.ArrayList;

public class CapaLogica {

    static final String NOMBRE_ARCHIVO_CLIENTE = "Clientes.txt";
    static final String NOMBRE_ARCHIVO_CAMISA = "Camisas.txt";
    static final String NOMBRE_ARCHIVO_CATALOGO = "Catalogos.txt";
    private Catalogo catalog;

    public CapaLogica() {
catalog = new Catalogo();
    }


    public static boolean crearArchivoCliente() throws IOException {

        File f = new File(NOMBRE_ARCHIVO_CLIENTE);

        if (f.createNewFile()) {
            System.out.println("Archivo creado exitosamente !");
            return true;
        } else {
            System.out.println("El archivo ya existe!.");
            return false;
        }
    }

    public String registrarCliente(Cliente cliente) {

        try {
            FileWriter writer = new FileWriter(NOMBRE_ARCHIVO_CLIENTE, true);
            BufferedWriter buffer = new BufferedWriter(writer);

            buffer.write(cliente.toStringCSV());
            buffer.newLine();
            buffer.close();
            return "Cliente creado de manera correcta";
        } catch (IOException e) {
            e.printStackTrace();
            return "Error, problemas para crear el cliente";
        }
    }

    public ArrayList<Cliente> listarClientes() {
        ArrayList<Cliente> listarClientes = new ArrayList<>();

        try {
            //1. Abrir el archivo
            FileReader reader = new FileReader(NOMBRE_ARCHIVO_CLIENTE);
            BufferedReader buffer = new BufferedReader(reader);

            String registro;
            //2. Leer el archivo (ciclo for)
            while ((registro = buffer.readLine()) != null) {
                String[] partesCliente = registro.split(",");
                Cliente cliente = new Cliente(partesCliente[0], partesCliente[1], partesCliente[2],
                        partesCliente[3], partesCliente[4], partesCliente[5]);

                //5. Agregar el objeto al arreglo de estudiantes
                listarClientes.add(cliente);
            }
            //6. Retornar el arreglo de estudiantes
            return listarClientes;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;


    }

    public Cliente averiguarCliente(String cedula) {
        for (Cliente cliente : listarClientes()) {
            if (cedula.equals(cliente.getCedula())) {
                return cliente;
            }
        }
        return null;
    }
    //---------------------------------------------------------------------

    public static boolean crearArchivoCamisa() throws IOException {

        File c = new File(NOMBRE_ARCHIVO_CAMISA);

        if (c.createNewFile()) {
            System.out.println("Archivo creado exitosamente !");
            return true;
        } else {
            System.out.println("El archivo ya existe!.");
            return false;
        }
    }

    public String registrarCamisa(Camisa camisa) {

        try {
            FileWriter writer = new FileWriter(NOMBRE_ARCHIVO_CAMISA, true);
            BufferedWriter buffer = new BufferedWriter(writer);

            buffer.write(camisa.toStringCSV());
            buffer.newLine();
            buffer.close();
            return "camisa creada de manera correcta";
        } catch (IOException e) {
            e.printStackTrace();
            return "Error, problemas para crear la camisa";
        }
    }

    public ArrayList<Camisa> listarCamisas() {
        ArrayList<Camisa> listarCamisas = new ArrayList<>();

        try {
            //1. Abrir el archivo
            FileReader reader = new FileReader(NOMBRE_ARCHIVO_CAMISA);
            BufferedReader buffer = new BufferedReader(reader);

            String registro;
            //2. Leer el archivo (ciclo for)
            while ((registro = buffer.readLine()) != null) {
                String[] partesCamisa = registro.split(",");


                Camisa camisa = new Camisa(partesCamisa[0], partesCamisa[1], partesCamisa[2],
                        Integer.parseInt(partesCamisa[3]),
                        Double.parseDouble(partesCamisa[4]));

                //5. Agregar el objeto al arreglo de estudiantes
                listarCamisas.add(camisa);
            }
            //6. Retornar el arreglo de estudiantes
            return listarCamisas;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;


    }

    public Camisa averiguarCamisa(int id) {
        for (Camisa camisa : listarCamisas()) {
            if (id == camisa.getId()) {
                return camisa;
            }
        }
        return null;
    }

    public void agregarClientesCatalogo(Cliente cliente){
        catalog.addCliente(cliente);                                   //----------------------------------------
    }
//---------------------------------------------------------------------


    public String registrarCatalogo(Catalogo catalogo) {

        try {
            FileWriter writer = new FileWriter(NOMBRE_ARCHIVO_CATALOGO, true);
            BufferedWriter buffer = new BufferedWriter(writer);

            buffer.write(catalogo.toStringCSV());
            buffer.newLine();
            buffer.close();
            return "Catalogo creado de manera correcta";
        } catch (IOException e) {
            e.printStackTrace();
            return "Error, problemas para crear el catalogo";
        }
    }

    public static boolean crearArchivoCatalogo() throws IOException {

        File cat = new File(NOMBRE_ARCHIVO_CATALOGO);

        if (cat.createNewFile()) {
            System.out.println("Archivo creado exitosamente !");
            return true;
        } else {
            System.out.println("El archivo ya existe!.");
            return false;
        }
    }

    public ArrayList<Catalogo> listarCatalogos() {
        ArrayList<Catalogo> listarCatalogos = new ArrayList<>();

        try {
            //1. Abrir el archivo
            FileReader reader = new FileReader(NOMBRE_ARCHIVO_CATALOGO);
            BufferedReader buffer = new BufferedReader(reader);

            String registro;
            //2. Leer el archivo (ciclo for)
            while ((registro = buffer.readLine()) != null) {
                String[] partesCatalogo = registro.split(",");
                Catalogo catalogo = new Catalogo(partesCatalogo[0], partesCatalogo[1], partesCatalogo[2]);

                //5. Agregar el objeto al arreglo de estudiantes
                listarCatalogos.add(catalogo);
            }
            //6. Retornar el arreglo de estudiantes
            return listarCatalogos;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;


    }

    public Catalogo averiguarCatalogo(String id) {
        for (Catalogo catalogo : listarCatalogos()) {
            if (id.equals(catalogo.getId())) {
                return catalogo;
            }
        }
        return null;
    }
}