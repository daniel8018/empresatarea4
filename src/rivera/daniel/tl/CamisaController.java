package rivera.daniel.tl;

import rivera.daniel.bl.Camisa;
import rivera.daniel.dl.CapaLogica;

import java.io.IOException;
import java.util.ArrayList;

public class CamisaController {

    private CapaLogica logica;

    public CamisaController() {
        logica = new CapaLogica();
    }

    public String registrarCamisa(String tam, String descri, String color, int id, double precio) {
        Camisa camisa = new Camisa(tam, descri, color, id, precio);
        return logica.registrarCamisa(camisa);

    }

    public ArrayList<Camisa> listarCamisas() {
        return logica.listarCamisas(); }

    public boolean crearArchivoCamisas() throws IOException {
        if (logica.crearArchivoCamisa() == true) {
            System.out.println("");
            return true;
        } else {
            System.out.println("El archivo ya está creado");
            return false;

        }
    }

    public Camisa averiguarCamisa(int id) {
        return logica.averiguarCamisa(id);
    }

}